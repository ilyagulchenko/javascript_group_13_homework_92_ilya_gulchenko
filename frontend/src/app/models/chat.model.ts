export interface Message {
  _id: string,
  user: ChatUser,
  text: string,
  datetime: string
}

export interface ChatUser {
  _id: string,
  displayName: string
}
