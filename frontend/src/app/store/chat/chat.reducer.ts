import { ChatState } from '../types';
import { createReducer, on } from '@ngrx/store';
import { fetchMessages, updateActiveUsers } from './chat.actions';

const initialState: ChatState = {
  messages: [],
  activeUsers: [],
};

export const chatReducer = createReducer(
  initialState,
  on(fetchMessages, (state, {messages}) => ({...state, messages: [...state.messages, ...messages]})),
  on(updateActiveUsers, (state, {activeUsers}) => ({...state, activeUsers}))
)
