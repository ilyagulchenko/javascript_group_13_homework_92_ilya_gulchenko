import { createAction, props } from '@ngrx/store';
import { ChatUser, Message } from '../../models/chat.model';

export const fetchMessages = createAction('[Chat] Fetch Messages', props<{messages: Message[]}>());
export const updateActiveUsers = createAction('[Chat] Update Active Users', props<{activeUsers: ChatUser[]}>());
