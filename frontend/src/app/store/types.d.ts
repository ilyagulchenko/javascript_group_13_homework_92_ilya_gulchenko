import { LoginError, RegisterError, User } from '../models/user.model';
import { ChatUser, Message } from '../models/chat.model';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
}

export type ChatState = {
  messages: Message[],
  activeUsers: ChatUser[]
}

export type AppState = {
  users: UsersState,
  chat: ChatState
};
