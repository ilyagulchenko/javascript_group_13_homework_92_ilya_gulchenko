import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ChatUser, Message } from '../../models/chat.model';
import { User } from '../../models/user.model';
import { fetchMessages, updateActiveUsers } from '../../store/chat/chat.actions';

interface IncomingMessage {
  type: string,
  data: Message | Message[] | ChatUser[]
}


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {
  ws!: WebSocket;
  messages: Observable<Message[]>;
  activeUsers: Observable<ChatUser[]>;
  user: Observable<null | User>;
  userSub!: Subscription;
  token: string | null = null;
  messageText = '';

  constructor(private store: Store<AppState>) {
    this.messages = store.select(state => state.chat.messages);
    this.activeUsers = store.select(state => state.chat.activeUsers);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.userSub = this.user.subscribe(user => {
      if (user) {
        this.token = user.token;
        this.connect();
      } else {
        this.token = null;
        this.disconnect();
      }
    });
  }

  connect() {
    this.ws = new WebSocket('ws://localhost:8000/chat');

    this.ws.onopen = () => {
      this.ws.send(JSON.stringify({
        type: 'LOGIN',
        data: this.token
      }));
    };

    this.ws.onmessage = (msg) => {
      const decodedMessage: IncomingMessage = JSON.parse(msg.data);

      switch (decodedMessage.type) {
        case 'LIST_ACTIVE_USERS':
          const activeUsers = <ChatUser[]>decodedMessage.data;
          this.store.dispatch(updateActiveUsers({activeUsers}))
          break;
        case 'LAST_MESSAGES':
          const lastMessages = <Message[]>decodedMessage.data;
          this.store.dispatch(fetchMessages({messages: lastMessages}));
          break;
        case 'NEW_MESSAGE':
          const newMessage = <Message>decodedMessage.data;
          this.store.dispatch(fetchMessages({messages: [newMessage]}));
          break;
        default:
          console.error('Wrong incoming message type:', decodedMessage.type);
      }
    }
  }

  disconnect() {
    if (this.ws) {
      this.ws.close();
    }
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
    this.disconnect();
  }

  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND_MESSAGE',
      data: this.messageText
    }));
    this.messageText = '';
  }
}
