const User = require("../models/User");
const Message = require("../models/Message");

const activeUsers = {};

const sendToEverybody = message => {
    Object.keys(activeUsers).forEach(activeUserId => {
        const activeUser = activeUsers[activeUserId];
        activeUser.ws.send(JSON.stringify(message));
    })
};

const getAllActiveUsers = () => {
    return Object.keys(activeUsers).map(activeUserId => {
        const activeUser = activeUsers[activeUserId];

        return {
            _id: activeUser.user._id,
            displayName: activeUser.user.displayName
        };
    })
};

const sendActiveUsers = () => {
    const message = {
        type: 'LIST_ACTIVE_USERS',
        data: getAllActiveUsers()
    };

    sendToEverybody(message);
};

module.exports = async (ws, req) => {
    let userId = '';
    let currentUser = null;

  ws.on('message', async msg => {
      try {
          const decodedMessage = JSON.parse(msg);

          switch (decodedMessage.type) {
              case 'LOGIN':
                  const user = await User.findOne({token: decodedMessage.data});

                  if (!user) {
                      return ws.close();
                  }

                  userId = user._id.toString();
                  currentUser = user;
                  activeUsers[userId] = {ws, user};

                  sendActiveUsers();

                  const last30Messages = await Message.find().limit(30).populate('user', '_id displayName');

                  ws.send(JSON.stringify({
                      type: 'LAST_MESSAGES',
                      data: last30Messages
                  }));
                  break;
              case 'SEND_MESSAGE':
                  if (!userId) return;

                  const message = new Message({
                      user: currentUser,
                      text: decodedMessage.data
                  });

                  await message.save();

                  sendToEverybody({
                      type: 'NEW_MESSAGE',
                      data: {
                          _id: message._id,
                          user: {_id: currentUser._id, displayName: currentUser.displayName},
                          text: message.text,
                          datetime: message.datetime
                      }
                  })
                  break;
              default:
                  console.error('Wrong incoming message type: ', decodedMessage.type);
          }

          ws.on('close', () => {
              if (userId) {
                  delete activeUsers[userId];
                  sendActiveUsers();
                  console.log('User disconnected: ', currentUser.displayName);
              } else {
                  console.log('Not logged in User disconnected');
              }
          })
      } catch (e) {
          console.error('Incoming message error:', e);
      }
  })
};
