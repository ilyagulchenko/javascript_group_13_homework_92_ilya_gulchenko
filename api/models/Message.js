const mongoose = require("mongoose");

const MessageSchema = new mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    },
    text: {
        type: String,
        required: true
    },
    datetime: {
        type: Date,
        default: () => new Date()
    }
})

const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;
