const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const { nanoid } = require("nanoid");

const run = async () => {
    await mongoose.connect(config.mongoConfig.db, config.mongoConfig.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    await User.create({
        email: 'user@mail.ru',
        password: '123',
        displayName: 'John Alli',
        role: 'user',
        token: nanoid(),
    }, {
        email: 'moderator@mail.ru',
        password: '123',
        displayName: 'David Sim',
        role: 'moderator',
        token: nanoid(),
    })

    await mongoose.connection.close();
};

run().catch(e => console.error(e));
