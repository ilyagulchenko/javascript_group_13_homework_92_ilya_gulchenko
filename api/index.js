const express = require('express');
const expressWs = require('express-ws');
const cors = require('cors');
const mongoose = require("mongoose");
const config = require('./config');
const chat = require('./app/chat');
const users = require('./app/users');

const app = express();
expressWs(app);

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/users', users);
app.ws('/chat', chat);

const run = async () => {
    await mongoose.connect(config.mongoConfig.db, config.mongoConfig.options);

    app.listen(port, () => {
        console.log(`Server starts on ${port} port`);
    });

    process.on('exit', () => {
        mongoose.disconnect();
    });
}

run().catch(e => console.error(e));
