const rootPath = __dirname;

module.exports = {
    rootPath,
    mongoConfig: {
        db: 'mongodb://localhost/chat',
        options: {useNewUrlParser: true},
    }
}
